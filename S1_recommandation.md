# Fonctionnalité 5 : Un système de recommandation de morceaux


Il s'agit ici de mettre en place un système de recommandation de morceaux de musique à partir d'un fichier audio ou d'un micro.


Le principe sera le suivant :

+ Vous lancez votre interface graphique qui vous donne le choix entre micro ou fichier et qui permet de choisir le fichier dans ce deuxième cas.
+ Vous lancer l'application de reconnaissance et vous récupérez le résultat.
+ Vous analyser le résultat et vous créer une liste de morceaux *proches* du morceau reconnu (e.g. même auteur, même genre, ...)


Nous avons maintenant fini notre MVP. Et nous pouvons passer à l'[objectif 2](TemplateProject_Shazam.md).

